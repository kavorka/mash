using System.Collections.Generic;

namespace Mash.Infrastructure.Commands
{
    public interface ICommandBus
    {
        void Submit<TCommand>(TCommand command) where TCommand: ICommand;
    }
}