using System;
using System.Collections.Generic;
using Mash.Infrastructure.Exceptions;

namespace Mash.Infrastructure.Commands
{
    public class CommandBus : ICommandBus
    {
        private IServiceProvider serviceProvider;
        public CommandBus(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }
        public void Submit<TCommand>(TCommand command) where TCommand : ICommand
        {
            var handler = (ICommandHandler<TCommand>) serviceProvider.GetService(typeof(ICommandHandler<TCommand>));
            if (!((handler != null) && handler is ICommandHandler<TCommand>))
            {
                throw new CommandHandlerNotFoundException($"Commandhandler for commmand: {command.ToString()} not found");
            }
            handler.Execute(command);
        }
    }
}