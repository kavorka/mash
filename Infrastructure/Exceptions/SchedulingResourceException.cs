using System;

namespace Mash.Infrastructure.Exceptions
{
    public class SchedulingResourceException : Exception
    {
        public SchedulingResourceException()
        {
        }

        public SchedulingResourceException(string message)
            : base(message)
        {
        }
    }
}