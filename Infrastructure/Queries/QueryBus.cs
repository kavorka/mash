using System;
using System.Collections.Generic;

namespace Mash.Infrastructure.Queries
{
    public class QueryBus : IQueryBus
    {
        private IServiceProvider serviceProvider;

        public QueryBus(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public TResult Submit<TResult>(IQuery<TResult> query)
        { 
            var handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult));
            dynamic handler  = serviceProvider.GetService(handlerType);
            return handler.Execute((dynamic)query);
        }
    }
}