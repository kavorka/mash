using System.Collections.Generic;


namespace Mash.Infrastructure.Queries
{
    public interface IQueryBus
    {
        TResult Submit<TResult>(IQuery<TResult> query);
    }
}