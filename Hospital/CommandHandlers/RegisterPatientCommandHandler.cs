using System;
using Mash.Hospital.Commands;
using Mash.Hospital.Interfaces;
using Mash.Hospital.Models;
using Mash.Infrastructure.Commands;

namespace Mash.Hospital.CommandHandlers
{
    public class RegisterPatientCommandHandler : ICommandHandler<RegisterPatientCommand>
    {
        private IPatientScheduler patientScheduler;
        private IPatientRepository patientRepository;

        public RegisterPatientCommandHandler(IPatientRepository patientRepository, IPatientScheduler patientScheduler)
        {
            this.patientRepository = patientRepository;
            this.patientScheduler = patientScheduler;
        }

        public void Execute(RegisterPatientCommand command)
        {
            var patient = new Patient(){
                Name = command.Name,
                Condition = command.Condition
            };

            this.patientRepository.Save(patient);
            this.patientScheduler.SchedulePatient(patient);
        }
    }
}