namespace Mash.Hospital.Enums
{
    public enum Role
    {
        Oncologist,
        GeneralPractitioner
    }
}