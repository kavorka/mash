namespace Mash.Hospital.Enums
{
    public enum Topology
    {
        NotApplicable,
        HeadAndNeck,
        Breast
    }
}