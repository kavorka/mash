namespace Mash.Hospital.Enums
{
    public enum Capability
    {
        Advanced,
        Simple
    }
}