using Mash.Hospital.Interfaces;
using Mash.Hospital.Models;
using Mash.Infrastructure.Commands;

namespace Mash.Hospital.Commands
{
    public class RegisterPatientCommand: ICommand
    {
        public string Name { get; set; }
        public Condition Condition { get; set; }
    }
}