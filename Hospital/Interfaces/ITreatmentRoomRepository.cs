using System.Collections.Generic;
using Mash.Hospital.Models;

namespace Mash.Hospital.Interfaces
{
    public interface ITreatmentRoomRepository
    {
        IEnumerable<TreatmentRoom> GetTreatmentRooms(); 
    }
}