using System.Collections.Generic;
using Mash.Hospital.Enums;
using Mash.Hospital.Models;

namespace Mash.Hospital.Interfaces
{
    public interface IDoctorRepository
    {
        IEnumerable<Doctor> GetDoctors(); 
        IEnumerable<Doctor> GetDoctorsWithRole(Role role);
    }
}