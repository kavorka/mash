using Mash.Hospital.Models;

namespace Mash.Hospital.Interfaces
{
    public interface IPatientScheduler
    {
        void SchedulePatient(Patient patient);
    }
}