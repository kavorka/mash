using System;
using System.Collections.Generic;
using System.Linq;
using Mash.Hospital.Models;

namespace Mash.Hospital.Interfaces
{
    public interface IConsultationRepository
    {
        IEnumerable<Consultation> GetConsultations();
        ILookup<DateTime, Consultation> GetConsultationsDateLookup();
        void Save(Consultation Consultation);
    }
}