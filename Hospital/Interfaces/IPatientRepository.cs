using System.Collections.Generic;
using Mash.Hospital.Models;

namespace Mash.Hospital.Interfaces
{
    public interface IPatientRepository
    {
        IEnumerable<Patient> GetPatients();
        void Save(Patient patient);
    }
}