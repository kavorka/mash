using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Mash.Hospital.Enums;
using Mash.Hospital.Interfaces;
using Mash.Hospital.Models;
using Mash.Infrastructure.Exceptions;

namespace Mash.Hospital.Services
{
    public class PatientScheduler : IPatientScheduler
    {
        private object down = new object();
        private ITreatmentRoomRepository TreatmentRoomRepository { get; set; }
        private IDoctorRepository DoctorRepository { get; set; }
        private IConsultationRepository consultationRepository { get; set; }


        public PatientScheduler(
            IDoctorRepository DoctorRepository,
            ITreatmentRoomRepository TreatmentRoomRepository,
            IConsultationRepository consultationRepository)
        {
            this.DoctorRepository = DoctorRepository;
            this.TreatmentRoomRepository = TreatmentRoomRepository;
            this.consultationRepository = consultationRepository;
        }

        public void SchedulePatient(Patient patient)
        {
            switch (patient.Condition.Type)
            {
                case ConditionType.Flu:
                    ScheduleFluPatient(patient);
                    break;
                case ConditionType.Cancer:
                    ScheduleCancerPatient(patient);
                    break;
                default:
                    throw new NotSupportedException($"Condition {patient.Condition.Type} is not supported!");
            }
        }

        private void ScheduleFluPatient(Patient patient)
        {
            var schedulingConstraints = new SchedulingConstraints()
            {
                PossibleTreatmentRooms = TreatmentRoomRepository.GetTreatmentRooms().Where(r => !r.HasTreatmentMachine),
                PossibleDoctors = DoctorRepository.GetDoctorsWithRole(Role.GeneralPractitioner)
            };

            BookConsultation(patient, schedulingConstraints);
        }

        private void ScheduleCancerPatient(Patient patient)
        {
            var schedulingConstraints = new SchedulingConstraints();
            switch (patient.Condition.Topology)
            {
                case Topology.HeadAndNeck:
                    schedulingConstraints.PossibleTreatmentRooms = TreatmentRoomRepository.GetTreatmentRooms().Where(
                    r => r.HasTreatmentMachine &&
                    r.TreatmentMachine.Capability == Capability.Advanced);
                    break;
                case Topology.Breast:
                    schedulingConstraints.PossibleTreatmentRooms = TreatmentRoomRepository.GetTreatmentRooms().Where(r => r.HasTreatmentMachine);
                    break;
                default:
                    throw new NotSupportedException($"Topology: {patient.Condition.Topology} is not supported for cancer patients!");
            }

            schedulingConstraints.PossibleDoctors = DoctorRepository.GetDoctorsWithRole(Role.Oncologist);

            BookConsultation(patient, schedulingConstraints);
        }

        private void BookConsultation(
            Patient patient,
            SchedulingConstraints schedulingConstraints)
        {
            if (!ValidateConstraints(schedulingConstraints))
            {
                return;
            }

            lock (down)
            {
                var consultationDate = DateTime.Today.AddDays(1);
                var consultationDateLookup = consultationRepository.GetConsultationsDateLookup();
                ScheduleConsultation(consultationDate, patient, schedulingConstraints, consultationDateLookup);
            }
        }

        private bool ValidateConstraints(SchedulingConstraints schedulingConstraints)
        {
            List<string> errors = new List<string>();
            if (!schedulingConstraints.PossibleDoctors.Any())
            {
                errors.Add("no doctor with correct role working in hospital");
            }
            if (!schedulingConstraints.PossibleTreatmentRooms.Any())
            {
                errors.Add("no treatmentroom for condition in hospital");
            }
            if (errors.Any())
            {
                throw new SchedulingResourceException($"Error: {String.Join(",", errors)}");
            }

            return !errors.Any();
        }

        private void ScheduleConsultation(
            DateTime consultationDate,
            Patient patient,
            SchedulingConstraints schedulingConstraints,
            ILookup<DateTime, Consultation> consultationDateLookup)
        {
            var consultationsForDate = consultationDateLookup[consultationDate];
            var availableDoctors = schedulingConstraints.PossibleDoctors.Except(consultationsForDate.Select(c => c.Doctor));
            var availableRooms = schedulingConstraints.PossibleTreatmentRooms.Except(consultationsForDate.Select(c => c.TreatmentRoom));

            if (availableDoctors.Any() && availableRooms.Any())
            {
                var consultation = new Consultation()
                {
                    RegistrationDate = DateTime.Today,
                    ConsultationDate = consultationDate,
                    Doctor = availableDoctors.First(),
                    TreatmentRoom = availableRooms.First(),
                    Patient = patient

                };
                consultationRepository.Save(consultation);
            }
            else
            {
                ScheduleConsultation(consultationDate.AddDays(1), patient, schedulingConstraints, consultationDateLookup);
            }
        }
    }
}