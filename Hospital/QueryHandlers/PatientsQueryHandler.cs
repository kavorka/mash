using System;
using System.Collections.Generic;
using Mash.Hospital.Interfaces;
using Mash.Hospital.Models;
using Mash.Hospital.Queries;
using Mash.Infrastructure.Queries;

namespace Mash.Hospital.QueryHanlders
{
    public class PatientsQueryHandler: IQueryHandler<PatientsQuery, IEnumerable<Patient>>
    {
        private IPatientRepository patientRepository;
        public PatientsQueryHandler(IPatientRepository patientRepository)
        {
            this.patientRepository = patientRepository;
        }

        public IEnumerable<Patient> Execute(PatientsQuery query)
        {
            return patientRepository.GetPatients();
        }
    }
}