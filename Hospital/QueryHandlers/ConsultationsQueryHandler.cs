using System;
using System.Collections.Generic;
using Mash.Hospital.Interfaces;
using Mash.Hospital.Models;
using Mash.Hospital.Queries;
using Mash.Infrastructure.Queries;

namespace Mash.Hospital.QueryHanlders
{
    public class ConsultationsQueryHandler : IQueryHandler<ConsultationsQuery, IEnumerable<Consultation>>
    {
        private IConsultationRepository consultationRepsitory;
        public ConsultationsQueryHandler(IConsultationRepository consultationRepsitory)
        {
            this.consultationRepsitory = consultationRepsitory;
        }

        public IEnumerable<Consultation> Execute(ConsultationsQuery query)
        {
            return consultationRepsitory.GetConsultations();
        }
    }
}