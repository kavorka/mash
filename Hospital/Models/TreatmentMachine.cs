using Mash.Hospital.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Mash.Hospital.Models
{
    public class TreatmentMachine
    {
        public string Name { get; set; }
        
        public Capability Capability { get; set; }
    }
}