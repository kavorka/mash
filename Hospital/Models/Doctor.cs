using System.Collections.Generic;
using Mash.Hospital.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Mash.Hospital.Models
{
    public class Doctor
    {
        public string Name { get; set; }
        
        public IEnumerable<Role> Roles { get; set; }
    }
}