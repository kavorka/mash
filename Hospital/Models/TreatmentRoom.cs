
namespace Mash.Hospital.Models
{
    public class TreatmentRoom
    {
        public string Name { get; set; }
        public TreatmentMachine TreatmentMachine { get; set; }

        public bool HasTreatmentMachine 
        {
            get { return TreatmentMachine != null; }
        }
    }
}