using System;
using System.Collections.Generic;

namespace Mash.Hospital.Models
{
    public class SchedulingConstraints
    {
        public IEnumerable<Doctor> PossibleDoctors { get; set; }
        public IEnumerable<TreatmentRoom> PossibleTreatmentRooms { get; set; }
    }
}