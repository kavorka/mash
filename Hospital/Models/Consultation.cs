using System;

namespace Mash.Hospital.Models
{
    public class Consultation
    {
        public DateTime RegistrationDate { get; set; }
        public DateTime ConsultationDate { get; set; }
        public TreatmentRoom TreatmentRoom { get; set; }
        public Doctor Doctor { get; set; }
        public Patient Patient { get; set; }
    }
}