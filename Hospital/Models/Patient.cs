using Mash.Hospital.Enums;
using Mash.Hospital.Interfaces;

namespace Mash.Hospital.Models
{
    public class Patient
    {
        public string Name { get; set; }
        public Condition Condition { get; set; }

    }
}