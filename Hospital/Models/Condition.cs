using Mash.Hospital.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Mash.Hospital.Models
{
    public class Condition
    {
        public ConditionType Type { get; set; }

        public Topology Topology { get; set; }
    }
}