using System;
using System.Collections.Generic;
using System.Linq;
using Mash.Hospital.Models;
using Mash.Hospital.Interfaces;

namespace Mash.Hospital.Repositories
{
    public class ConsultationRepository : IConsultationRepository
    {
        private List<Consultation> Consultations { get; set; } = new List<Consultation>();

        public IEnumerable<Consultation> GetConsultations()
        {
            return Consultations;
        }

        public ILookup<DateTime, Consultation> GetConsultationsDateLookup()
        {
            return Consultations.ToLookup(c => c.ConsultationDate);
        }

        public void Save(Consultation consultation)
        {
            Consultations.Add(consultation);
            //Could send event here that a consultation is scheduled
        }
    }
}