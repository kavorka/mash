using System;
using System.Collections.Generic;
using Mash.Hospital.Interfaces;
using Mash.Hospital.Models;

namespace Mash.Hospital.Repositories
{
    public class PatientRepository: IPatientRepository
    {
        private List<Patient> Patients { get; set; } = new List<Patient>();

        public IEnumerable<Patient> GetPatients()
        {
            return this.Patients;
        }

        public void Save(Patient patient)
        {
            this.Patients.Add(patient);
            //Could send event here to notify that patient is registerd
        }
    }
}