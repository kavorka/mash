using System;
using System.Collections.Generic;
using System.Linq;
using Mash.Hospital.Enums;
using Mash.Hospital.Interfaces;
using Mash.Hospital.Models;

namespace Mash.Hospital.Repositories
{
    public class DoctorRepository: IDoctorRepository
    {
        private List<Doctor> Doctors { get; set; } = new List<Doctor>();

        public DoctorRepository()
        {
            InitDoctorsData();
        }

        private void InitDoctorsData()
        {
            this.Doctors = new List<Doctor>(){
                new Doctor(){ Name="John", Roles = new List<Role>(){Role.Oncologist}},
                new Doctor(){ Name="Anna", Roles = new List<Role>(){Role.GeneralPractitioner}},
                new Doctor(){ Name="Peter", Roles = new List<Role>(){Role.Oncologist,Role.GeneralPractitioner}},
            };
        }

        public IEnumerable<Doctor> GetDoctors()
        {
            return this.Doctors;
        }

        public IEnumerable<Doctor> GetDoctorsWithRole(Role role)
        {
            return this.Doctors.Where(d => d.Roles.Any(r => r == role));
        }
    }
}
    