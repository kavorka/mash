using System;
using System.Collections.Generic;
using Mash.Hospital.Enums;
using Mash.Hospital.Interfaces;
using Mash.Hospital.Models;

namespace Mash.Hospital.Repositories
{
    public class TreatmentRoomRepository: ITreatmentRoomRepository
    {
        private IEnumerable<TreatmentRoom> TreatmentRooms { get; set; } = new List<TreatmentRoom>();

        public TreatmentRoomRepository()
        {
            InitTreatmentRoomData();
        }

        private void InitTreatmentRoomData()
        {
            this.TreatmentRooms = new List<TreatmentRoom>(){
                new TreatmentRoom(){ Name="one", TreatmentMachine = new TreatmentMachine(){ Name="Elekta", Capability = Capability.Advanced}},
                new TreatmentRoom(){ Name="two", TreatmentMachine = new TreatmentMachine(){ Name="Varian", Capability = Capability.Advanced}},
                new TreatmentRoom(){ Name="three", TreatmentMachine = new TreatmentMachine(){ Name="MM50", Capability = Capability.Simple}},
                new TreatmentRoom(){ Name="Four"},
                new TreatmentRoom(){ Name="Five"}
            };
        }

        public IEnumerable<TreatmentRoom> GetTreatmentRooms()
        {
            return this.TreatmentRooms;
        }
    }
}
    