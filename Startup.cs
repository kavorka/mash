﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Mash.Hospital.CommandHandlers;
using Mash.Hospital.Commands;
using Mash.Hospital.Queries;
using Mash.Hospital.Interfaces;
using Mash.Hospital.Models;
using Mash.Hospital.QueryHanlders;
using Mash.Hospital.Repositories;
using Mash.Hospital.Services;
using Mash.Infrastructure.Commands;
using Mash.Infrastructure.Queries;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Swashbuckle.AspNetCore.Swagger;

namespace Mash
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc().AddJsonOptions(opt =>
            {
                //Only added to make json responses more readable
                var settings = opt.SerializerSettings;
                settings.Converters.Add(new StringEnumConverter { CamelCaseText = true });
            });

            // Add application services.
            services.AddSingleton<IPatientRepository, PatientRepository>();
            services.AddSingleton<IDoctorRepository, DoctorRepository>();
            services.AddSingleton<ITreatmentRoomRepository, TreatmentRoomRepository>();
            services.AddSingleton<IConsultationRepository, ConsultationRepository>();
            services.AddSingleton<IPatientScheduler, PatientScheduler>();
            services.AddScoped<ICommandBus, CommandBus>();
            services.AddScoped<IQueryBus, QueryBus>();

            //Other DI container like simple injector or autofac could add with auto descovery and support decorator pattern
            services.AddScoped<ICommandHandler<RegisterPatientCommand>, RegisterPatientCommandHandler>();
            services.AddScoped(typeof(IQueryHandler<PatientsQuery,IEnumerable<Patient>>), typeof(PatientsQueryHandler));
            services.AddScoped(typeof(IQueryHandler<ConsultationsQuery,IEnumerable<Consultation>>), typeof(ConsultationsQueryHandler));

            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Mash Hospital", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Mash Hospital V1");
            });
        }
    }
}
