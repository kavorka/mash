using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Mash.Controllers.Filters
{
    //simple exception filter to capture exception thrown internaly 
    public class ApiExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            if (exception != null) 
            {
                context.HttpContext.Response.StatusCode = 500;
                context.Result = new JsonResult(exception);
            }
            //Should add logging as well...
        }
    }
}
