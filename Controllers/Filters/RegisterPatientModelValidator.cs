using System.Collections.Generic;
using System.Linq;
using Mash.Hospital.Commands;
using Mash.Hospital.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Mash.Controllers.Filters
{
    //Very simple example of model validation
    public class RegisterPatientModelValidatorAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
             foreach (var argument in context.ActionArguments.Values.Where(v => v is RegisterPatientCommand))
            {
                var model = argument as RegisterPatientCommand;
                var errors = new List<string>();
                if(string.IsNullOrEmpty(model.Name))
                {
                    errors.Add("Patient name is missing!");
                }
                if(model.Condition == null)
                {   
                    errors.Add("Patient condition null is not allowed");

                }
                if(errors.Any())
                {
                    context.HttpContext.Response.StatusCode = 400;
                    context.Result = new JsonResult(errors);
                }
            }
        }
    }
}