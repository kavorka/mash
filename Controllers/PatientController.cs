using System;
using System.Collections.Generic;
using Mash.Controllers.Filters;
using Mash.Hospital.Commands;
using Mash.Hospital.Interfaces;
using Mash.Hospital.Models;
using Mash.Hospital.Queries;
using Mash.Hospital.Services;
using Mash.Infrastructure.Commands;
using Mash.Infrastructure.Queries;
using Microsoft.AspNetCore.Mvc;

namespace Mash.Controllers
{
    [Route("api/[controller]")]
    public class PatientController : Controller
    {
        private ICommandBus commandBus;
        private IQueryBus queryBus;

        public PatientController(ICommandBus commandBus, IQueryBus queryBus)
        {
            this.commandBus = commandBus;
            this.queryBus = queryBus;
        }
        // GET api/patient/list
        [HttpGet]
        [Route("list")]
        public IEnumerable<Patient> Get()
        {
            return queryBus.Submit(new PatientsQuery());
        }

        // POST api/patient/register
        [HttpPost]
        [Route("register")]
        [ApiException]
        [RegisterPatientModelValidator]
        public void Post([FromBody]RegisterPatientCommand command)
        {
            this.commandBus.Submit(command);
        }
    }
}
