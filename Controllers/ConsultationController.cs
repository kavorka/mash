using System;
using System.Collections.Generic;
using Mash.Hospital.Commands;
using Mash.Hospital.Interfaces;
using Mash.Hospital.Models;
using Mash.Hospital.Queries;
using Mash.Hospital.Services;
using Mash.Infrastructure.Queries;
using Microsoft.AspNetCore.Mvc;

namespace Mash.Controllers
{
    [Route("api/[controller]")]
    public class ConsultationController : Controller
    {
        private IQueryBus queryBus;
        public ConsultationController(IQueryBus queryBus)
        {
            this.queryBus = queryBus;
        }

        // GET api/Consultation/list
        [HttpGet]
        [Route("list")]
        public IEnumerable<Consultation> GetConsultations()
        {
            return queryBus.Submit(new ConsultationsQuery());
        }
    }
}
